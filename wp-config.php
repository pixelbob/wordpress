<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'math');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}p|(+|UI]|Zt$|^tuw?9${@lMcl$;tL?=j29jh|wK>k6 -V-fs`ZyeAVTTMo6~TR');
define('SECURE_AUTH_KEY',  'b]tOGYTM%:ShS8aS^l1e?z[a9,05Y$<F?+Xs%6A37|)Fa<W2A6)8+#We *c%KJlS');
define('LOGGED_IN_KEY',    '>Kfc-@DxP3^+#EX9s@)4r/4)b&d|e1(ZS>%0XD}x^.!keg|+JsNZ6q8t@ji<vIx|');
define('NONCE_KEY',        '>>+VR=``O1Pf<=eRLTHYD2n4.Z4FVO}<N.D+9*ZLW{UWl>.@czAfT1q3V)7YYP|6');
define('AUTH_SALT',        '5z0 QpB)7k|{mNGMrmvdTP>_6^D>@Z8b fIFw%o#-,;f|^-%:A$*Q(I8J%Hi /Mq');
define('SECURE_AUTH_SALT', 'o<lg^+P?`Uq:&1|j<&XsK5lKmj9iy$soEPK|m;tDwrzrQJ-3/%cGPQkxRHr9,$K-');
define('LOGGED_IN_SALT',   's~iJ][u7|Eu,.CFeq Op=x5=tT93d/|+FzBM4kq0YY3J+IeR>63K-l7=J`.(v2;s');
define('NONCE_SALT',       'H|J0^>M~W7gu>Z4$<C2.@6ABN@eF#A^6qK~5TY[-)H|TrCkExTT_i^]}^x;H}._:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
