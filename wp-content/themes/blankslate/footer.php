<div class="clear"></div>
</div> <!--container END-->
<footer id="footer" role="contentinfo">
<div class="testimonials">
	<img src="/wp-content/themes/blankslate/img/hex-light-blue.png" class="hex-icon" />
	<div class="testimonial">
		<?php echo do_shortcode("[ic_do_testimonials group='dmti-testimonials' ajax_on='no']"); ?>
		</br>
		<a href="/testimonials/">See All Testimonials</a>
	</div>
</div>
<div class="footer-rule"></div>
<ul class="footer-nav">
	<li><a href="/professional-development/">Professional<br/>Development<br/><img src="/wp-content/themes/blankslate/img/icon-development.png" /></a></li>
	<li><a href="/curricular-resources/">Curricular<br/>Resources<br/><img src="/wp-content/themes/blankslate/img/icon-resources.png" /></a></li>
	<li><a href="/assessment/">Assessment<br/><img src="/wp-content/themes/blankslate/img/icon-assessment.png" /></a></li>
</ul>

<div id="copyright">
<?php echo sprintf( __( '%1$s %2$s %3$s.', 'blankslate' ), '&copy;', date( 'Y' ), esc_html( get_bloginfo( 'name' ) )  ); ?> &nbsp;&nbsp; <a href="/terms/">Terms of Use</a> &nbsp;&nbsp;  <a href="/privacy/">Privacy Policy</a> &nbsp;&nbsp; <a href="/contact/">Contact Us</a>
</div>
</footer>
</div> <!--wrapper END-->
<?php wp_footer(); ?>
</body>
</html>