<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( ' | ', true, 'right' ); ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic|Open+Sans:300,600' rel='stylesheet' type='text/css'>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- 
<?php if (is_page('Curricular Resources') || is_page('Assessment')) { ?>
<div class="subscribe-button hoverboard"><a href="/register/"><img src="/wp-content/themes/blankslate/img/subscribe-button.png" /></a></div>
<?php } ?>
 -->
<div id="wrapper" class="hfeed">
<header id="header" role="banner">
<div id="header-inner">
<a href="#" class="open-panel"><div class="icon-mobile-menu"></div></a>

<nav id="menu" role="navigation">
<a href="#" class="close-panel"><div class="icon-mobile-menu-close"></div></a>
<ul class="menu">
<?php if (!is_page('Home')) { ?>
<li><a href="/"><img src="/wp-content/themes/blankslate/img/icon-home.png" /></a></li>
<?php } ?>
<li><a href="/professional-development/">Professional Development</a></li>
<li><a href="/curricular-resources/">Curricular Resources</a></li>
<li><a href="/assessment/">Assessment</a></li>
</ul>
</nav>
<ul class="util-menu">
<!-- 
<li><a href="/sign-in/">Sign In</a> | </li>
<li><a href="/register/">Register</a> | </li>
 -->
<li class="header-right"><a href="/contact/">Contact Us</a> <a href="http://resources.dmtinstitute.com" target="_blank" class="button">Resources Login</a></li>
</ul>
</div>
</header>
<div id="container">